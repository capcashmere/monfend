<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request, Validator, App\User;

class pagesCtrl extends Controller
{
    public function index(Request $request)
    {
        return view('cs.landing');
    }

    public function accountSetup (Request $request) {
        if ($request->isMethod('post')) {
            
            $v = Validator::make($request->input(), [
                'email' => 'required|email|unique:users',
                'full_name' => 'required',
                'phone_number' => 'required|digits:10',
                'company_name' => 'required'
            ], [
                'email.required' => 'What\'s your e-mail ?',
                'email.email' => 'Your e-mail is incorrect',
                'email.unique' => 'This e-mail already exists',
                'full_name.required' => 'What\'s your name ?',
                'phone_number.required' => 'What\'s your phone number ?',
                'phone_number.digits' => 'Your phone number is incorrect',
                'company_name.required' => 'What\'s your company name ?'
            ]);

            if($v->fails()) {
                return response(['error' => true, 'messages' => $v->messages()], 422);
            }

            $u = new User;
            $u->email = $request->input('email');
            $u->full_name = $request->input('full_name');
            $u->phone_number = $request->input('phone_number');
            $u->company_name = $request->input('company_name');

            if($u->save()){
                return ['success' => true];
            }

            return response(['error' => true], 422);
        }

        return view('cs.accountSetup');
    }
}
