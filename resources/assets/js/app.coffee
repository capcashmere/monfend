Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('.token').getAttribute('content');
signUpCtrl = new Vue({
    el: '.signUpCtrl',
    data: {
        user: {
            email: null,
            full_name: null,
            phone_number: null,
            company_name: null,
        },
        currentView: 'first'

        loading: false
    },
    mounted: ->
        $('input,select').on('focus', (e) ->
            el = e.currentTarget.parentElement
            $('.input.selected').removeClass('selected')
            $(el).addClass('selected');

            if $(el).hasClass('errored')
                $(el).find('.msg-error').addClass('show');
            
        ).on('blur', (e) ->
            el = e.currentTarget.parentElement
            $(el).removeClass('selected')
            if $(el).hasClass('errored')
               $(el).find('.msg-error.show').remove();
               $(el).find('.error-input-icon').remove();
               $(el).removeClass('errored');
            
        )
    methods: {
        doSignup : (e)->
            self = this
            self.loading = true
            $('.input.errored').removeClass('errored');
            $('.input .error-input-icon, .input .msg-error').remove();

            this.$http.post('/users/sign-up', this.user).then(
                (r) =>
                    b = r.body
                    self.loading = false
                    if b.success?
                        self.currentView = 'success'
                (e) =>
                    b = e.body
                    if b.error?
                        e_msg = b.messages
                        $.each(e_msg, (k,v) ->
                            tmp = '<i class="icon-attention-circled error-input-icon"></i>' + '<div class="msg-error">' + v[0] + '</div>';        
                            el = $('.input.' + k);
                            el.addClass('errored');
                            el.append(tmp);
                        )
                    
                    

                    self.loading = false
            );
            
    }
})