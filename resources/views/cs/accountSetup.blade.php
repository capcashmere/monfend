<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <meta name="token" class="token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ mix('css/_common.css') }}">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body class="actstup_app">
    <div class="wrapper signUpCtrl">
        <div class="container">
            <div class="large-5 columns nop lside mt">
                <h1 class="nop logo" style="background-image: url({{ route('asset', 'logo.png') }});"><a href="#">Monitor</a></h1>
                <h2 class="nop title" v-if="currentView == 'first'">
                    Create an account
                </h2>
                <p class="nop tagline" v-if="currentView == 'first'">
                    Lorem ipsum dolor sit amet perilis
                </p>
                <div class="form-signup" v-if="currentView == 'first'" v-cloak>
                    <form action="{{ route('login') }}" v-on:submit.prevent="doSignup">
                        <div class="input email">
                            <div class="input-icon">
                                <i class="icon-mail"></i>
                            </div>
                            <input type="text" v-model="user.email" placeholder="Work email">
                        </div>
                       <div class="input full_name">
                            <div class="input-icon">
                                <i class="icon-user"></i>
                            </div>
                            <input type="text" v-model="user.full_name" placeholder="Full name">                            
                        </div>
                        <div class="input phone_number">
                           <div class="input-icon">
                                <i class="icon-phone"></i>
                            </div>
                            <input type="text" v-model="user.phone_number" placeholder="Phone Number">                                
                            </div>
                        <div class="input company_name">
                            <div class="input-icon">
                                <i class="icon-globe"></i>
                            </div>
                            <input type="text" v-model="user.company_name" placeholder="Company name">
                        </div>

                        <div class="input company_size">
                            <select name="" id="">
                                <option value="0">Company size</option>
                            </select>
                            <i class="icon-angle-down ico"></i>
                        </div>

                        <div class="input agency">
                            <select name="" id="">
                                <option value="0">Are you an agency?</option>
                            </select>
                            <i class="icon-angle-down ico"></i>
                        </div>

                        <div class="cgu-text">
                            By clicking on "Create account" you accept <a href="#">Terms of service.</a>
                        </div>
                        <div class="input_s">
                            <button type="submit"><span v-show="!loading">Request a demo</span> <i class="loading-btn" v-cloak v-bind:class="{'hide' : !loading}" style="background-image: url({{ route('asset', 'loading-blank.svg') }});"></i></button>
                        </div>
                    </form>
                </div>

                <div class="success-msg" v-cloak v-if="currentView == 'success'">
                    <i class="icon-success" style="background-image: url({{ route('asset', 'checked.svg') }});"></i>
                    <div class="title-success">
                        Lorem ipsum created
                    </div>
                    <p class="nop">
                        Iste sapiente quasi facilis itaque blanditiis.
                    </p>
                    <i class="icon-loading" style="background-image: url({{ route('asset', 'loading.svg') }});"></i>
                </div>

              
                
                <footer>
                    © 2018 Monitor. All rights reserved.
                </footer>
            </div>
        </div>
    </div>
    <script src="{{ mix('js/_common.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>