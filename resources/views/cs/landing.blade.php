<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ mix('css/_common.css') }}">
    <link rel="stylesheet" href="{{ mix('css/landing.css') }}">
</head>
<body>
    <script src="{{ mix('js/_common.js') }}"></script>
    <script src="{{ mix('js/landing.js') }}"></script>
</body>
</html>