<?php

Route::group(['domain' => env('domain')], function() {
    Route::get('/', ['as' => 'home', 'uses' => 'pagesCtrl@index']);
    Route::match(['GET', 'POST'],'/users/sign-up', ['as' => 'login', 'uses' => 'pagesCtrl@accountSetup']);
});

Route::group(['domain' => 's.'.env('domain')], function() {
    Route::get('/assets/{file}', ['as' => 'asset']);
    Route::get('/static/{file}', ['as' => 'static']);
});
