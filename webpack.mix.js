let mix = require('laravel-mix');

mix.scripts([
    'resources/assets/js/vendor/jquery.js',
    'resources/assets/js/vendor/underscore-min.js',
    'resources/assets/js/vendor/vue.min.js',
    'resources/assets/js/vendor/vue-resource.min.js'
], 'public/js/_common.js');

mix.js('resources/assets/js/app.coffee', 'public/js').webpackConfig({
    module: {
        rules: [
            { test: /\.coffee$/, loader: 'coffee-loader' }
        ]
    }
});

mix.js('resources/assets/js/landing.coffee', 'public/js').webpackConfig({
    module: {
        rules: [
            { test: /\.coffee$/, loader: 'coffee-loader' }
        ]
    }
});

mix.styles([
    'resources/assets/css/foundation.css',
    'resources/assets/css/capricons.css'
], 'public/css/_common.css');

mix.sass('resources/assets/sass/app.scss', 'public/css');
mix.sass('resources/assets/sass/landing.scss', 'public/css');

mix.version();